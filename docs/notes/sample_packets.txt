packet( adapter("A"), ethernet([17, 20]), header("TCP", ["172.17.41.41", 8000], ["172.17.11.23", 8080], [] )).
packet( adapter("B"), ethernet([]), header("ICMP", ["172.17.41.41", 8000], ["172.17.11.23", 80], [8, 1])).


% how to encode "any" ?
% ethernet([], []) is this a good way to ignore ethernet in a packet?
% protocol("TCP" or "UDP", source, dest)
% protocol("ICMP", type, code)
% ethernet([protocol_ids], [lower, upper])

% header(protocol, [source_address, source_port], [destination_address, destination_port], [icmp_type, icmp_code])
% the [icmp_type, icmp_code] list may be empty
% protocol ids: {"ICMP":1, "TCP":6, "UDP": 17}

% if the user enters an invalid port when querying, then drop silently.


screen(packet(adapter("A"), ethernet([17, 20])), Result).