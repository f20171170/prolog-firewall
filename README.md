# Prolog-Firewall

## Introduction
This project is a prolog implementation of an *adapted* version of the IBM Firewall Rules Language, completed as part of the *BITS CS F214 course (first semester)*.  

**Dependencies**:
- [SWI Prolog](www.swi-prolog.org/download/stable)

## Usage

### Overview  
Essentially this project consists of two main components which the user can interact with.
1. **The plfw_config.pl file**:  
    This file is where the user can use certain predicates (defined in the *"Configuration"*) to be able to define their own firewall.  

2. **The engine.pl file**:  
    This is where the user can run their queries. By executing this program in SWI Prolog,
    the user can then begin posting queries (please see *"Querying"*) to see which packets may be passed and which ones may not be (further if a packet was rejected, the user will be given all of the reasons as to why).

### Configuration
Part of the core logic behind this program is that by default every packet is accepted, the user can then use *reject* and *drop* predicates to define what packets must be blocked.  

*"Rejecting"* a packet means that the packet must be blocked, and the reasons for blocking must be displayed. *"Droping"* a packet means that the packet must be silently blocked (i.e. the reason for dropping will not be displayed).

***Predicates:***  
1. **reject(Clause)**.  
Reject any packets satisfying the Clause. A clause is one of the predicates defined from #3 onwards.
2. **drop(Clause)**.  
Drop any packets satisfying the Clause. A clause is one of the predicates defined from #3 onwards.
1. **rreject(Clause)**.  
Reject any packets satisfying the Clause. A clause is one of the predicates defined from #3 onwards.
2. **ddrop(Clause)**.  
Drop any packets satisfying the Clause. A clause is one of the predicates defined from #3 onwards.
3. **adapter(AdapterLetter).**  
Block any packets which would hit the specified adapter. AdapterLetter is a letter (in the form of a string) between **"A"** and **"P"**. When using in *rreject* or *ddrop*, AdapterLetter may be a range e.g. **"A-G"**.
4. **ethernet(EthernetProtocol, VLANNumber).**  
If a packet is inbound via. ethernet, then it will give a protocol number (EthernetProtocol: 1-999) and a VLANNumber (1-256); both are integers. When using in *rreject* or *ddrop*, EthernetProtocol may be a range e.g. **[1, 108]**.
5. **ipv4(Protocol, SourceAddress, DestinationAddress).**  
Block any IPV4 packets which are from SourceAddress and headed for DestinationAddress under the said Protocol. Protocol may be "TCP", "UDP" or "ICMP". SourceAddress and Destination must be valid IPv4 addresses. When using in *rreject* or *ddrop*, SourceAddress and DestinationAddress may be ranges e.g. **"172.17.21.1-172.17.21.8"**, **NOTE:** only the last segment of the address may be ranged, for example, in the preceding example, *172.17.21* must remain fixed.
6. **tcp(SourcePort, DestinationPort).**  
Block any TCP/IP packets which are from SourcePort and headed for DestinationPort. SourcePort and DestinationPort are a valid integer between 0 and 65535 (inclusive).
When using in *rreject* or *ddrop*, SourcePort and DestinationPort may be ranges e.g. **[2000, 8000]**.
7. **udp(SourcePort, DestinationPort).**  
Block any UDP/IP packets which are from SourcePort and headed for DestinationPort. SourcePort and DestinationPort are a valid integer between 0 and 65535 (inclusive). When using in *rreject* or *ddrop*, SourcePort and DestinationPort may be ranges e.g. **[2000, 8000]**.
8. **icmp(ICMPType, ICMPCode).**
Block any packets containing ICMP data with an ICMP type of ICMPType (int) and an ICMP code of ICMPCode (int).When using in *rreject* or *ddrop*, ICMPType and ICMPCode may be ranges e.g. **[1, 8]**.

### Sample Configuration
```
%%% Adapter Tests %%%
reject(adapter("A")).
% drop(adapter("B")). "%" implies that the line has been commented out.

%%% Ethernet Tests %%%
reject(ethernet(1, 1)).
drop(ethernet(1, 2)).

%%% IPv4 Tests %%%
%reject(ipv4(protocol("UDP"))).
%drop(ipv4(protocol("ICMP"))).
reject(ipv4("TCP", "any", "0.0.0.0")).
reject(ipv4("any", "0.0.0.4", "any")).
reject(ipv4("any", "0.0.0.1", "0.0.0.2")).
drop(ipv4("any", "0.0.0.1", "0.0.0.3")).

%%% TCP/UDP Tests %%%
reject(tcp("any", 80)).
drop(udp("any", 80)).
reject(tcp(9000, 9001)).
reject(tcp(90, "any")).
drop(udp(90, "any")).
%reject(tcp("any", "any")).

%%% ICMP Tests %%%
reject(icmp(80, 22)).
drop(icmp(90, 100)).
```  


### Querying
In order to query, run engine.pl in SWI Prolog. Then you may begin "screening" network packets with any one the following query syntatic structures:  

1. **screen(AdapterLetter, ethernet(EthernetProtocol, VLANNumber), Protocol, SourceAddr, SourcePort, DestAddr, DestPort, ICMPType, ICMPCode).**  

2. **screen(AdapterLetter, ethernet(EthernetProtocol, VLANNumber), Protocol, SourceAddr, SourcePort, DestAddr, DestPort).**  

3. **screen(AdapterLetter, Protocol, SourceAddr, SourcePort, DestAddr, DestPort, ICMPType, ICMPCode).**  

4. **screen(AdapterLetter, Protocol, SourceAddr, SourcePort, DestAddr, DestPort).**  

After typing out any one of the above queries and then hitting *"enter"*, press *"."* to end the query. A result of *"true"* impiles that the packet passes and a result of *"false"* implies that the packet was blocked (either rejected or droppped).

## Developers
- Hemanth V. Alluri (2017A7PS1170P)  
- Nevin Thomas (2017A7SPS1175P)
