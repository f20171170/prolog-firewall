emptyList([]).


%Final validation
	
validation(Adapter,EProto,EVid,Protocol,SrcAddr, SrcPort, DestAddr, DestPort,Imcp_Type,Imcp_Code):-
	
	valid_AdapterId(Adapter), 
	valid_ethernet(EProto,EVid),validProtocolType(Protocol), valid_IPv4List(SrcAddr,DestAddr),validPort(SrcPort),validPort(DestPort),validIMCP_Type_Code(Imcp_Type,Imcp_Code).

valid_ethernet(Proto,Vid):-
	between(1,999,Proto),
	between(1,999,Vid).
	
valid_AdapterId(X):-
	Anum is "A",
	Bnum is "Z",
	Xnum is X,
	between(Anum,Bnum,Xnum).

valid_IPv4List(Src,Dest):-
	split_string(Src,"." , "",SrcL),
	split_string(Dest,"." , "",DestL),
	valid_IPv4Addr(SrcL),
	valid_IPv4Addr(DestL).

	
valid_IPv4Addr([ X1,X2,X3,X4 | T ]):-
		atom_number(X1,N1),
		atom_number(X2,N2),
		atom_number(X3,N3),
		atom_number(X4,N4),
		N1>=0, N1=<256,
		N2>=0, N2=<256,
		N3>=0, N3=<256,
		N4>=0, N4=<256,
		emptyList(T).
	
		
validProtocolType(X):-
	X="TCP"; X="UDP".
	
validProtocolIDList([ProtocolID]):- 
	integer(ProtocolID),
	ProtocolID>=1, ProtocolID=<256.
validProtocolIDList([ProtocolID | T]):-
	integer(ProtocolID),
	ProtocolID>=1, ProtocolID=<256,
	validProtocolIDList(T).

validVLANRange([R1,R2|End]):-
	R1>=1,R1=<256,
	R2>=1,R2=<256,
	emptyList(End).

validPort(Port):-
	Port>=1,Port=<65355 .

validIMCP_Type_Code(X,Y):-
	X>=1,X=<256,
	Y>=1,Y=<256.	
	


		
	