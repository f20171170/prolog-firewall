
%Enter user input in here 


%%% Adapter Tests %%%	
reject(adapter("A")).
drop(adapter("B")).
rreject(adapter("D-F")).
j(1).
%%% Ethernet Tests %%%
reject(ethernet(1, 1)).
drop(ethernet(1, 2)).

%%% IPv4 Tests %%%
%reject(ipv4(protocol("UDP"))).
%drop(ipv4(protocol("ICMP"))).
reject(ipv4("TCP", "any", "0.0.0.0")).
ddrop(ipv4("TCP", "0.0.0.2-0.0.0.3", "0.0.1.0")).
reject(ipv4("any", "0.0.0.4", "any")).
rreject(ipv4("any", "1.0.0.1-1.0.0.6", "0.0.0.2")).
drop(ipv4("any", "0.0.0.1", "0.0.0.3")).

%%% TCP/UDP Tests %%%
%/
reject(tcp("any", 80)).
drop(udp("any", 80)).
reject(tcp(9000, 9001)).
reject(tcp(90, "any")).
drop(udp(90, "any")).
%reject(tcp("any", "any")).

%%% ICMP Tests %%%
reject(icmp(80, 22)).
drop(icmp(90, 100)).
%//


