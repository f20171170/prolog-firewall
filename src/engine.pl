﻿:- discontiguous drop/1.
:- discontiguous reject/1.
:- discontiguous ipv4/1.
:- discontiguous check_ports/3.
:- discontiguous check_ports/4.
:-discontiguous rreject/1.
:- [valid_code,plfw_config].

screen(AdapterLetter, ethernet(Proto, Vid), Protocol, SourceAddr, SourcePort, DestAddr, DestPort, ICMPType, ICMPCode):-
	% This double evaluation scheme is a logical hack which would allow us to figure out every reason for why a packet is rejected.
	% The first round is for printing out all of the reasons and the second one is for getting the end result of true/false.
	% /logic: Dis(Ф) ʌ Con(Ф) → Con(Ф)
	% where Dis(Ф) implies disjunction of clauses in Ф, and Con(Ф) implies a conjuction of the clauses in Ф.
	% drawback: computation must be performed twice.
	validation(AdapterLetter, Proto, Vid, Protocol, SourceAddr, SourcePort, DestAddr, DestPort, ICMPType, ICMPCode),
	(
		check_adapter(AdapterLetter);
		check_ethernet(Proto, Vid);
		check_ipv4(Protocol, SourceAddr, DestAddr);
		check_icmp(ICMPType, ICMPCode);
		check_ports(Protocol, SourcePort, DestPort)
	),
	(
		check_adapter(AdapterLetter, "NO_PRINT"),
		check_ethernet(Proto, Vid, "NO_PRINT"),
		check_ipv4(Protocol, SourceAddr, DestAddr, "NO_PRINT"),
		check_icmp(ICMPType, ICMPCode, "NO_PRINT"),
		check_ports(Protocol, SourcePort, DestPort, "NO_PRINT")
	).

screen(AdapterLetter, ethernet(Proto, Vid), Protocol, SourceAddr, SourcePort, DestAddr, DestPort):-
	validation(AdapterLetter,Proto,Vid,Protocol,SourceAddr,SourcePort,DestAddr,DestPort,1,1),
	(
		check_adapter(AdapterLetter);
		check_ethernet(Proto, Vid);
		check_ipv4(Protocol, SourceAddr, DestAddr);
		check_ports(Protocol, SourcePort, DestPort)
	),
	(
		check_adapter(AdapterLetter, "NO_PRINT"),
		check_ethernet(Proto, Vid, "NO_PRINT"),
		check_ipv4(Protocol, SourceAddr, DestAddr, "NO_PRINT"),
		check_ports(Protocol, SourcePort, DestPort, "NO_PRINT")
	).

screen(AdapterLetter, Protocol, SourceAddr, SourcePort, DestAddr, DestPort, ICMPType, ICMPCode):-
	validation(AdapterLetter,1,1,Protocol,SourceAddr,SourcePort,DestAddr,DestPort,ICMPType,ICMPCode),
	(
		check_adapter(AdapterLetter);
		check_ipv4(Protocol, SourceAddr, DestAddr);
		check_icmp(ICMPType, ICMPCode);
		check_ports(Protocol, SourcePort, DestPort)
	),
	(
		check_adapter(AdapterLetter, "NO_PRINT"),
		check_ipv4(Protocol, SourceAddr, DestAddr, "NO_PRINT"),
		check_icmp(ICMPType, ICMPCode, "NO_PRINT"),
		check_ports(Protocol, SourcePort, DestPort, "NO_PRINT")
	).

screen(AdapterLetter, Protocol, SourceAddr, SourcePort, DestAddr, DestPort):-
	validation(AdapterLetter,1,1,Protocol,SourceAddr,SourcePort,DestAddr,DestPort,1,1),
	(
		check_adapter(AdapterLetter);
		check_ipv4(Protocol, SourceAddr, DestAddr);
		check_ports(Protocol, SourcePort, DestPort)

	),
	(
		check_adapter(AdapterLetter, "NO_PRINT"),
		check_ipv4(Protocol, SourceAddr, DestAddr, "NO_PRINT"),
		check_ports(Protocol, SourcePort, DestPort, "NO_PRINT")
	).

check_adapter(Letter):-
	\+ (reject(adapter("any")), write("Rejected: All adapters have been disabled.\n")),
	\+ (reject(adapter(Letter)), write("Rejected: Adapter has been protected.\n")),
	\+ (drop(adapter(Letter)); drop(adapter("any"))).

check_adapter(Letter, "NO_PRINT"):-
	\+ (reject(adapter("any"))),
	\+ (reject(adapter(Letter))),
	\+ (drop(adapter(Letter)); drop(adapter("any"))).

check_ethernet(Proto, Vid):-
	\+ (reject(ethernet("any")), write("Rejected: Ethernet has been disabled.\n")),
	\+ (reject(ethernet(Proto, Vid)), write("Rejected: Ethernet has been protected.\n")),
	\+ (drop(ethernet(Proto, Vid)); drop(ethernet("any"))).

check_ethernet(Proto, Vid, "NO_PRINT"):-
	\+ (reject(ethernet("any"))),
	\+ (reject(ethernet(Proto, Vid))),
	\+ (drop(ethernet(Proto, Vid)); drop(ethernet("any"))).

check_ipv4(Protocol, SourceAddr, DestAddr):-
	\+ (reject(ipv4(Protocol, "any", "any")), write("Rejected: Protocol has been disabled.\n")),
	\+ (reject(ipv4(Protocol, SourceAddr, "any")), write("Rejected: Source Address has been blocked under protocol.\n")),
	\+ (reject(ipv4(Protocol, "any", DestAddr)), write("Rejected: Destination Address has been protected under protocol.\n")),
	\+ (reject(ipv4(Protocol, SourceAddr, DestAddr)), write("Rejected: Address pair has been banned under protocol.\n")),

	\+ (reject(ipv4("any", "any", "any")), write("Rejected: IPv4 has been disabled.\n")),
	\+ (reject(ipv4("any", SourceAddr, "any")), write("Rejected: Source Address has been blocked.\n")),
	\+ (reject(ipv4("any", "any", DestAddr)), write("Rejected: Destination Address has been protected.\n")),
	\+ (reject(ipv4("any", SourceAddr, DestAddr)), write("Rejected: Address pair has been banned.\n")),


	\+ ((drop(ipv4(Protocol, "any", "any")); drop(ipv4(Protocol, SourceAddr, "any")); drop(ipv4(Protocol, "any", DestAddr)); drop(ipv4(Protocol, SourceAddr, DestAddr)))),
	\+ ((drop(ipv4("any", "any", "any")); drop(ipv4("any", SourceAddr, "any")); drop(ipv4("any", "any", DestAddr)); drop(ipv4("any", SourceAddr, DestAddr)))).

check_ipv4(Protocol, SourceAddr, DestAddr, "NO_PRINT"):-
	\+ (reject(ipv4(Protocol, "any", "any"))),
	\+ (reject(ipv4(Protocol, SourceAddr, "any"))),
	\+ (reject(ipv4(Protocol, "any", DestAddr))),
	\+ (reject(ipv4(Protocol, SourceAddr, DestAddr))),

	\+ (reject(ipv4("any", "any", "any"))),
	\+ (reject(ipv4("any", SourceAddr, "any"))),
	\+ (reject(ipv4("any", "any", DestAddr))),
	\+ (reject(ipv4("any", SourceAddr, DestAddr))),


	\+ ((drop(ipv4(Protocol, "any", "any")); drop(ipv4(Protocol, SourceAddr, "any")); drop(ipv4(Protocol, "any", DestAddr)); drop(ipv4(Protocol, SourceAddr, DestAddr)))),
	\+ ((drop(ipv4("any", "any", "any")); drop(ipv4("any", SourceAddr, "any")); drop(ipv4("any", "any", DestAddr)); drop(ipv4("any", SourceAddr, DestAddr)))).


check_icmp(ICMPType, ICMPCode):-
	\+ ((reject(ipv4("ICMP")); reject(icmp("any"))), write("Rejected: ICMP has been disabled.\n")),
	\+ (reject(icmp(ICMPType, ICMPCode)), write("Rejected: ICMP packet has been blocked.\n")),
	\+ (drop(icmp("any")); drop(icmp(ICMPType, ICMPCode)); drop(ipv4("ICMP"))).

check_icmp(ICMPType, ICMPCode, "NO_PRINT"):-
	\+ ((reject(ipv4("ICMP")); reject(icmp("any")))),
	\+ (reject(icmp(ICMPType, ICMPCode))),
	\+ (drop(icmp("any")); drop(icmp(ICMPType, ICMPCode)); drop(ipv4("ICMP"))).

check_tcp(SourcePort, DestPort):-
	\+ ((reject(tcp("any", "any")); reject(ipv4(protocol("TCP")))), write("Rejected: TCP has been disabled.\n")),
	\+ (reject(tcp(SourcePort, "any")), write("Rejected: Source Port has been blocked.\n")),
	\+ (reject(tcp("any", DestPort)), write("Rejected: Destination Port has been protected.\n")),
	\+ (reject(tcp(SourcePort, DestPort)), write("Rejected: Port pair has been banned.\n")),
	\+ ((drop(tcp("any", "any")); drop(tcp(SourcePort, "any")); drop(tcp("any", DestPort)); drop(tcp(SourcePort, DestPort)); drop(ipv4(protocol("TCP"))))).

check_tcp(SourcePort, DestPort, "NO_PRINT"):-
	\+ ((reject(tcp("any", "any")); reject(ipv4(protocol("TCP"))))),
	\+ (reject(tcp(SourcePort, "any"))),
	\+ (reject(tcp("any", DestPort))),
	\+ (reject(tcp(SourcePort, DestPort))),
	\+ ((drop(tcp("any", "any")); drop(tcp(SourcePort, "any")); drop(tcp("any", DestPort)); drop(tcp(SourcePort, DestPort)); drop(ipv4(protocol("TCP"))))).

check_udp(SourcePort, DestPort):-
	\+ ((reject(udp("any", "any")); reject(ipv4(protocol("UDP")))), write("Rejected: UDP has been disabled.\n")),
	\+ (reject(udp(SourcePort, "any")), write("Rejected: Source Port has been blocked.\n")),
	\+ (reject(udp("any", DestPort)), write("Rejected: Destination Port has been protected.\n")),
	\+ (reject(udp(SourcePort, DestPort)), write("Rejected: Port pair has been banned.\n")),
	\+ ((drop(udp("any", "any")); drop(udp(SourcePort, "any")); drop(udp("any", DestPort)); drop(udp(SourcePort, DestPort));drop(ipv4(protocol("UDP"))))).

check_udp(SourcePort, DestPort, "NO_PRINT"):-
	\+ ((reject(udp("any", "any")); reject(ipv4(protocol("UDP"))))),
	\+ (reject(udp(SourcePort, "any"))),
	\+ (reject(udp("any", DestPort))),
	\+ (reject(udp(SourcePort, DestPort))),
	\+ ((drop(udp("any", "any")); drop(udp(SourcePort, "any")); drop(udp("any", DestPort)); drop(udp(SourcePort, DestPort));drop(ipv4(protocol("UDP"))))).

check_ports("TCP", SourcePort, DestPort):-
	check_tcp(SourcePort, DestPort).

check_ports("TCP", SourcePort, DestPort, "NO_PRINT"):-
	check_tcp(SourcePort, DestPort, "NO_PRINT").

check_ports("UDP", SourcePort, DestPort):-
	check_udp(SourcePort, DestPort).

check_ports("UDP", SourcePort, DestPort, "NO_PRINT"):-
	check_udp(SourcePort, DestPort, "NO_PRINT").

check_ports("ICMP", _, _):-
% icmp has no such special rules in this language
	true.

check_ports("ICMP", _, _, _):-
	true.
	
%The ranges part
%0.0.0.4-0.0.0.7
isRangeAddr(Addr):-
	split_string(Addr,"-","",List),
	List=[_,_|[]].

reject(ipv4(P,X,Y)):-
	rreject(ipv4(P,AdL,Adr)),
	isRangeAddr(AdL),not(isRangeAddr(Adr)),
	%write("1 called "),
	split_string(AdL,"-","",List),
	List=[Ad1,Ad2|[]],
	split_string(Ad1,".","",AdL1),
	AdL1=[Ad11,Ad12,Ad13,Ad14|_],
	split_string(Ad2,".","",AdL2),
	AdL2=[_,_,_,Ad24|_],
	atom_number(Ad14,R1),
	atom_number(Ad24,R2),
	string_concat(Ad11,".",G0),
	string_concat(G0,Ad12,G1),
	string_concat(G1,".",G2),
	string_concat(G2,Ad13,G3),
	string_concat(G3,".",G4),
	string_concat(G4,Var,X),
	Y=Adr,
	atom_number(Var,V2),
	between(R1,R2,V2).
	
%ipv4("anything",range)
	
reject(ipv4(P,X,Y)):-
	rreject(ipv4(P,Adr,AdL)),
	isRangeAddr(AdL), not(isRangeAddr(Adr)),
	%write("2 called "),
	split_string(AdL,"-","",List),
	List=[Ad1,Ad2|[]],
	split_string(Ad1,".","",AdL1),
	AdL1=[Ad11,Ad12,Ad13,Ad14|_],
	split_string(Ad2,".","",AdL2),
	AdL2=[_,_,_,Ad24|_],
	atom_number(Ad14,R1),
	atom_number(Ad24,R2),
	string_concat(Ad11,".",G0),
	string_concat(G0,Ad12,G1),
	string_concat(G1,".",G2),
	string_concat(G2,Ad13,G3),
	string_concat(G3,".",G4),
	string_concat(G4,Var,Y),
	%write(Y),
	X=Adr,
	atom_number(Var,V2),
	between(R1,R2,V2).
	
%ipv4(range,range)
	
reject(ipv4(P,X,Y)):-
	rreject(ipv4(P,ADL,ADL2)),
	isRangeAddr(ADL), isRangeAddr(ADL2),
	%write("3 called "),
	split_string(ADL,"-","",List),
	List=[Ad1,Ad2|[]],
	split_string(Ad1,".","",AdL1),
	AdL1=[Ad11,Ad12,Ad13,Ad14|_],
	split_string(Ad2,".","",AdL2),
	AdL2=[_,_,_,Ad24|_],
	atom_number(Ad14,R1),
	atom_number(Ad24,R2),
	%write(R1),
	%write(R2),
	string_concat(Ad11,".",G0),
	string_concat(G0,Ad12,G1),
	string_concat(G1,".",G2),
	string_concat(G2,Ad13,G3),
	string_concat(G3,".",G4),
	string_concat(G4,Var,X),
	%write(X),
	atom_number(Var,V2),
	between(R1,R2,V2),
	split_string(ADL2,"-","",List1),
	List1=[ADD1,ADD2|[]],
	split_string(ADD1,".","",ADL21),
	ADL21=[AD11,AD12,AD13,AD14|_],
	split_string(ADD2,".","",ADL22),
	ADL22=[_,_,_,AD24|_],
	atom_number(AD14,R01),
	atom_number(AD24,R02),
	%write(R1),
	%write(R2),
	string_concat(AD11,".",G00),
	string_concat(G00,AD12,G01),
	string_concat(G01,".",G02),
	string_concat(G02,AD13,G03),
	string_concat(G03,".",G04),
	string_concat(G04,Var1,Y),
	%write(Y),
	atom_number(Var1,V222),
	between(R01,R02,V222).

%list of protocols-ethernet	

reject(ethernet(X,Vid)):-
	rreject(ethernet([L1,L2],Vid)),
	between(L1,L2,X).
	
%tcp/ucp ports
	
reject(tcp(X,Y)):-
	rreject(tcp([L1,H1],Port1)),
	Y=Port1,
	between(L1,H1,X).
reject(tcp(X,Y)):-
	rreject(tcp(Port2,[L2,H2])),
	X=Port2,
	between(L2,H2,Y).
reject(tcp(X,Y)):-
	rreject(tcp([L1,H1],[L2,H2])),
	between(L1,H1,X),
	between(L2,H2,Y).
	
reject(udp(X,Y)):-
	rreject(udp([L1,H1],Port1)),
	Y=Port1,
	between(L1,H1,X).
reject(udp(X,Y)):-
	rreject(udp(Port2,[L2,H2])),
	X=Port2,
	between(L2,H2,Y).
reject(udp(X,Y)):-
	rreject(udp([L1,H1],[L2,H2])),
	between(L1,H1,X),
	between(L2,H2,Y).

%IMCP   

reject(icmp(X,Y)):-
	rreject(icmp([L1,H1],Port1)),
	Y=Port1,
	between(L1,H1,X).	
reject(icmp(X,Y)):-
	rreject(icmp(Port2,[L2,H2])),
	X=Port2,
	between(L2,H2,Y).
reject(icmp(X,Y)):-
	rreject(icmp([L1,H1],[L2,H2])),
	between(L1,H1,X),
	between(L2,H2,Y).
	
%Adapters

reject(adapter(X)):-
	rreject(adapter(Range)),
	not(X="any"),
	split_string(Range,"-","",[Lower,Upper]),
	L is Lower,
	H is Upper,
	M is X,
	between(L,H,M).
	
%DROP

drop(ipv4(P,X,Y)):-
	ddrop(ipv4(P,AdL,Adr)),
	isRangeAddr(AdL),not(isRangeAddr(Adr)),
	%write("1 called "),
	split_string(AdL,"-","",List),
	List=[Ad1,Ad2|[]],
	split_string(Ad1,".","",AdL1),
	AdL1=[Ad11,Ad12,Ad13,Ad14|_],
	split_string(Ad2,".","",AdL2),
	AdL2=[_,_,_,Ad24|_],
	atom_number(Ad14,R1),
	atom_number(Ad24,R2),
	string_concat(Ad11,".",G0),
	string_concat(G0,Ad12,G1),
	string_concat(G1,".",G2),
	string_concat(G2,Ad13,G3),
	string_concat(G3,".",G4),
	string_concat(G4,Var,X),
	Y=Adr,
	atom_number(Var,V2),
	between(R1,R2,V2).
	
%ipv4("anything",range)
	
drop(ipv4(P,X,Y)):-
	ddrop(ipv4(P,Adr,AdL)),
	isRangeAddr(AdL), not(isRangeAddr(Adr)),
	%write("2 called "),
	split_string(AdL,"-","",List),
	List=[Ad1,Ad2|[]],
	split_string(Ad1,".","",AdL1),
	AdL1=[Ad11,Ad12,Ad13,Ad14|_],
	split_string(Ad2,".","",AdL2),
	AdL2=[_,_,_,Ad24|_],
	atom_number(Ad14,R1),
	atom_number(Ad24,R2),
	string_concat(Ad11,".",G0),
	string_concat(G0,Ad12,G1),
	string_concat(G1,".",G2),
	string_concat(G2,Ad13,G3),
	string_concat(G3,".",G4),
	string_concat(G4,Var,Y),
	%write(Y),
	X=Adr,
	atom_number(Var,V2),
	between(R1,R2,V2).
	
%ipv4(range,range)
	
drop(ipv4(P,X,Y)):-
	ddrop(ipv4(P,ADL,ADL2)),
	isRangeAddr(ADL), isRangeAddr(ADL2),
	%write("3 called "),
	split_string(ADL,"-","",List),
	List=[Ad1,Ad2|[]],
	split_string(Ad1,".","",AdL1),
	AdL1=[Ad11,Ad12,Ad13,Ad14|_],
	split_string(Ad2,".","",AdL2),
	AdL2=[_,_,_,Ad24|_],
	atom_number(Ad14,R1),
	atom_number(Ad24,R2),
	%write(R1),
	%write(R2),
	string_concat(Ad11,".",G0),
	string_concat(G0,Ad12,G1),
	string_concat(G1,".",G2),
	string_concat(G2,Ad13,G3),
	string_concat(G3,".",G4),
	string_concat(G4,Var,X),
	%write(X),
	atom_number(Var,V2),
	between(R1,R2,V2),
	split_string(ADL2,"-","",List1),
	List1=[ADD1,ADD2|[]],
	split_string(ADD1,".","",ADL21),
	ADL21=[AD11,AD12,AD13,AD14|_],
	split_string(ADD2,".","",ADL22),
	ADL22=[_,_,_,AD24|_],
	atom_number(AD14,R01),
	atom_number(AD24,R02),
	%write(R1),
	%write(R2),
	string_concat(AD11,".",G00),
	string_concat(G00,AD12,G01),
	string_concat(G01,".",G02),
	string_concat(G02,AD13,G03),
	string_concat(G03,".",G04),
	string_concat(G04,Var1,Y),
	%write(Y),
	atom_number(Var1,V222),
	between(R01,R02,V222).

%list of protocols-ethernet	

drop(ethernet(X,Vid)):-
	ddrop(ethernet([L1,L2],Vid)),
	between(L1,L2,X).
	
%tcp/ucp ports
	
drop(tcp(X,Y)):-
	ddrop(tcp([L1,H1],Port1)),
	Y=Port1,
	between(L1,H1,X).
drop(tcp(X,Y)):-
	ddrop(tcp(Port2,[L2,H2])),
	X=Port2,
	between(L2,H2,Y).
drop(tcp(X,Y)):-
	ddrop(tcp([L1,H1],[L2,H2])),
	between(L1,H1,X),
	between(L2,H2,Y).
	
drop(udp(X,Y)):-
	ddrop(udp([L1,H1],Port1)),
	Y=Port1,
	between(L1,H1,X).
drop(udp(X,Y)):-
	ddrop(udp(Port2,[L2,H2])),
	X=Port2,
	between(L2,H2,Y).
drop(udp(X,Y)):-
	ddrop(udp([L1,H1],[L2,H2])),
	between(L1,H1,X),
	between(L2,H2,Y).

%IMCP   

drop(icmp(X,Y)):-
	ddrop(icmp([L1,H1],Port1)),
	Y=Port1,
	between(L1,H1,X).	
drop(icmp(X,Y)):-
	ddrop(icmp(Port2,[L2,H2])),
	X=Port2,
	between(L2,H2,Y).
drop(icmp(X,Y)):-
	ddrop(icmp([L1,H1],[L2,H2])),
	between(L1,H1,X),
	between(L2,H2,Y).
	
%Adapters

drop(adapter(X)):-
	ddrop(adapter(Range)),
	split_string(Range,"-","",[Lower,Upper]),
	L is Lower,
	H is Upper,
	\+ (X="any"),
	M is X,
	between(L,H,M).

